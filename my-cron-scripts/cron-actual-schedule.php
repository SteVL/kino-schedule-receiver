<?php

define('FOLDER_JSON', '/cron-schedule/');
define('QUANTITY_DAYS', 7);

//Рабочий вариант
define('CATALOG', '/home/k/kinovertru/public_html');
//Тестовый вариант
//define('CATALOG', 'E:/OSPanel/domains/kino-schedule-receiver');



//Формируем даты для запроса
$arrDate = array();
for ($i = 0; $i <= QUANTITY_DAYS; $i++) {
    $arrDate[$i] = (new DateTime('+' . $i . 'day'))->format('d.m.Y');
    //echo $arrDate[$i] . "<br>";
}



//Запрос JSON для всех дат
foreach ($arrDate as $date) {
    load_actual_schedule($date);
}
echo 'Данные сохранены в:' . "<br>";
echo CATALOG . FOLDER_JSON . "<br>";

//****Процедуры************
//Запрашивает актуальные данные по API
function load_actual_schedule($date) {
    $query_url = 'http://ticket.multidat.ru/api?apikey=99dd1fad74bc4b65bd6f8fba9b9ce19e&action=seance&date=' . $date;

    //Получаем статус ответа сервера
    $status = get_headers($query_url)[0];
    //Если успешный, то получаем содержимое
    if ($status == 'HTTP/1.0 200 OK') {
        $handle = fopen($query_url, "rb");
        $response = stream_get_contents($handle);
        fclose($handle);
        //если содержимое корректное
        if (isJSON($response)) {
            //сохраняем данные в кэш
            $file = $date . '.json';
            $path = CATALOG . FOLDER_JSON;
            file_put_contents($path . $file, $response);
            echo 'Расписание на ', $date, ' сохранено', "<br>";
        }
    } else {
        echo 'Расписание на ', $date, ' нет', "<br>";
    }
}

/**
 * Определяет является ли строка - JSON
 * @param str $string строка для проверки
 * @return bool
 */
function isJSON($string) {
    return ((is_string($string) &&
            (is_object(json_decode($string)) ||
            is_array(json_decode($string))))) ? true : false;
}
