<!doctype html>
<head>
    <!--    <meta http-equiv="Cache-Control" content="max-age=900, must-revalidate">-->
</head>
<?php
/*
  2. Если расписания нет на заданную дату, сервер возвращает ошибку в виде HTML страницы (Не JSON).
  Cоответственно, как вариант, проверить тип ответа.
  3. Получать данные на месяц вперёд.
 */

define('CACHE_DIR', dirname(__FILE__) . '/cron-schedule/');

//проверяем есть ли файлы в кэше
$b = dirIsEmpty(CACHE_DIR);
//var_dump('res = [' . $b . ']');
if ($b == true) {
    //если есть, то 
    //получаем файлы на актуальные даты
    $filesArr = get_actual_file_list();
    $arrDates = get_actual_dates($filesArr);

    //cортируем даты
   $arrDates=sort_dates($arrDates);

    //выводим все даты
    $count = count($arrDates);
    for ($i = 0; $i < $count; $i++) {
        echo $arrDates[$i], '<br>';
    }

    for ($i = 0; $i < $count; $i++) {
        //получаем содержимое
        $json = file_get_contents(CACHE_DIR . $arrDates[$i].'.json');
        echo '=== Расписание на ', $arrDates[$i], ' ===', '<br>';
        output_schedule($json);
    }
} else {
    print_r('Нет .json файлов в кэше');
}


//*************Процедуры*************

/**
 * Проверяет каталог на наличие JSON
 * @param type $dir
 * @return type bool
 */
function dirIsEmpty($dir) {
    $res = false;
    $filesArr = scandir($dir);
    #print_r($filesArr);
    //проверяем расширение
    $file = new SplFileInfo($filesArr[2]);
    $ext = $file->getExtension();
    //var_dump($ext);
    if ($ext == "json") {
        $res = true;
    }
    return (bool) $res;
}

/**
 * Получает список файлов из кэша с расписанием на дни, кроме прошедших
 * Полагается, что имя файла представлено в виде 08.08.2019.json
 * @return array массив актуальных файлов
 */
function get_actual_file_list() {
    $today = (new DateTime('+' . 0 . 'day'))->format('d.m.Y');
    $filesArr = [];
    $iterator = new DirectoryIterator(CACHE_DIR);
    foreach ($iterator as $file) {
        //если расширение json
        if ($file->getExtension() == 'json') {
            $fileNameNoExt = $file->getBasename('.json');
            //strtotime - конвертируем строку имени файла, что является датой, в тип времени.
            if (strtotime($fileNameNoExt) >= strtotime($today)) {
                //если имя-дата больше или равно сегодня, до добавляем в результ.массив
                $filesArr[] = $file->getFilename();
            }
        }
    }
    //var_dump($filesArr);
    return $filesArr;
}

/**
 * Получает массив дат из названий файлов
 * @param type $filesArr массив файлов с именами вида 08.08.2019.json
 * @return string массив дат
 */
function get_actual_dates($filesArr) {
    if (empty($filesArr)) {
        exit("Массив файлов пуст");
    }
    $arrDates = array();
    $count = count($filesArr);
    for ($i = 0; $i < $count; $i++) {
        $arrDates[] = basename($filesArr[$i], '.json');
    }
    return $arrDates;
}

/**
 * Сортировка дат
 * @param array str $arrDates массив строковых дат
 * @return array string отсортированный массив
 */
function sort_dates($arrDates) {
    //var_dump($arrDates);
    $tempArr=array();
    $count=count($arrDates);
    for($i=0;$i<$count; $i++){
        $tempArr[]=DateTime::createFromFormat('d.m.Y',$arrDates[$i]);
//        $date=DateTime::createFromFormat('d.m.Y',$arrDates[$i]);
//        $date=$date->format('d.m.Y');
//          echo '* ',$date;

    }
    //cортируем
    sort($tempArr);
    //пересоздаём строковый массив
    for($i=0;$i<$count; $i++){
        $tempArr[$i]=$tempArr[$i]->format('d.m.Y');
    }
    //var_dump($tempArr);
    //echo '<pre>',var_dump($tempArr),'</pre>';
    //sort($tempArr);
    //echo '<pre>',var_dump(sort($tempArr)),'</pre>';
    return $tempArr;
}

/**
 * Проверяет на JSON
 * @param string $string cтрока
 * @return bool
 */
function isJSON($string) {
    return ((is_string($string) &&
            (is_object(json_decode($string)) ||
            is_array(json_decode($string))))) ? true : false;
}

/**
 * Парсит JSON и выводит данные
 * @param str $json строка json
 */
function output_schedule($json) {
//ответ возвращает массив
    $json_arr = json_decode($json, true);

//Объект - фильм
    foreach ($json_arr as $item) {

//расписание включает форматы
        $schedule = $item['Schedule'];
//форматы (представлены ключами)
        $formats = array_keys($schedule);
        foreach ($formats as $format) {
                    $titleIsOut = false;
//каждый формат имеет несколько сеансов
            $seances = $schedule[$format];
            
//            отсортировать сеансы
            sort($seances);
//если есть disabled в cеансе, то название не выводим
//название фильма
//            echo $item['Film'], " ";
//            echo $format . "<br>";
            foreach ($seances as $seance) {
//сессия для ссылки на время
// echo $seance['session'], "<br>";
                if ($seance['session'] == 'disabled') {
                    continue;
                } else {

                    if (!$titleIsOut) {
                        echo $item['Film'], " ";
                        echo $format . "<br>";
                        $titleIsOut = true;
                    }
                    echo '<a href="#" md:widget="multidat" md:seanse="' . $seance['session'] . '" class="seance md_boxoffice">';
                    echo $seance['time'];
#$film['time'] = $seance['time'];
                    echo '</a>';
//сеанс имеет стоимость
                    $prices = $seance['price'];
//встречаются виды мест Обычное и Стандарт
//Если только обычные, массив без индексов, а если есть обычные и
// стандарт, то массив имеет индексы,
                    if (count($prices) == 5) {
//тип места только один - Обычное
                        echo ' ' . $prices['price'] . " р.", "<br>";
#$film['price'] = $prices['price'];
                    } else {
//иначе есть несколько типов мест
//обычное
//echo $prices[0]['name'], "<br>";
                        echo ' ' . $prices[0]['price'] . " р.", "<br>";
#$film['price'] = $prices[0]['price'];
//стандарт
// echo $prices[1]['name'], "<br>";
// echo $prices[1]['price']." р.", "<br>";
                    }
                }
            }
        }
    }
    echo "<br>";
//Для отладки
//echo '<pre>', print_r($json_arr), '</pre>';
}

echo'<script src="http://ticket.multidat.ru/tmpl/js/multidat.js?apikey=99dd1fad74bc4b65bd6f8fba9b9ce19e"></script>';
